package sendbird

// Sendbird header
const (
	SendbirdAPITokenHeaderKey      = "Api-Token"
	SendbirdContentTypeHeaderValue = "application/json, charset=utf8"
)

//Sendbird url prefix
const (
	constScheme  = "https"
	constHost    = "api-%s.sendbird.com"
	constVersion = "/v3"
)

//Sendbird urls template
const (
	//Users
	SendbirdURLUsers = `/users`

	//Group Channels
	SendbirdURLGroupChannels = `/group_channels`
)
