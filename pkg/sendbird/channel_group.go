package sendbird

type LastMessage struct {
	CreatedAt int64 `json:"created_at"`
	User      User  `json:"user"`
}

type GroupChannel struct {
	Channel

	IsDistinct         bool             `json:"is_distinct"`
	MemberCount        int              `json:"member_count"`
	Members            []User           `json:"members"`
	ReadReceipt        map[string]int64 `json:"read_receipt"`
	UnreadMessageCount int              `json:"unread_message_count"`
	LastMessage        LastMessage      `json:"last_message"`
}

type CreateGroupChannelRequest struct {
	Name       string   `json:"name,omitempty"`
	CoverURL   string   `json:"cover_url,omitempty"`
	CustomType string   `json:"custom_type,omitempty"`
	Data       string   `json:"data,omitempty"`
	UserIDs    []string `json:"user_ids,omitempty"`
	IsDistinct bool     `json:"is_distinct,omitempty"`
}

func (c *client) CreateAGroupChannelWithURL(r *CreateGroupChannelRequest) (GroupChannel, error) {
	result := GroupChannel{}

	if err := c.postAndReturnJSON(c.PrepareUrl(SendbirdURLGroupChannels), r, &result); err != nil {
		return GroupChannel{}, err
	}

	return result, nil
}
