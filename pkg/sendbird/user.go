package sendbird

import (
	"errors"
	"fmt"
	"mekari-chat-sendbird/pkg/error_utils"
	"mekari-chat-sendbird/pkg/logger_utils"
	"net/url"
	"strconv"
	"strings"
)

//Sendbird list limits
const (
	ListLimitUpperBound = 100
	ListLimitLowerBound = 1
	CurrentFilePath     = "[pkg][sendbird][user.go]"
)

type User struct {
	UserID      string `json:"user_id"`
	NickName    string `json:"nickname"`
	ProfileURL  string `json:"profile_url"`
	ProfileFile []byte `json:"profile_file"`
	AccessToken string `json:"access_token"`
	IsActive    bool   `json:"is_active"`
	IsOnline    bool   `json:"is_online"`
	LastSeenAt  int64  `json:"last_seen_at"`
}

func (r *ListUsersRequest) params() url.Values {
	q := make(url.Values)

	if r.Token != "" {
		q.Set("token", r.Token)
	}

	limit, err := strconv.Atoi(r.Limit)
	if err == nil {
		if limit >= ListLimitLowerBound && limit <= ListLimitUpperBound {
			q.Set("limit", r.Limit)
		}
	}

	if r.ActiveMode != "" {
		q.Set("active_mode", r.ActiveMode)
	}

	if r.ShowBot {
		q.Set("show_bot", "true")
	}

	if r.UserIDs != nil && len(r.UserIDs) > 0 {
		q.Set("user_ids", strings.Join(r.UserIDs, ","))
	}

	return q
}

type ListUsersRequest struct {
	Token      string   `json:"token,omitempty"`
	Limit      string   `json:"limit,omitempty"`
	ActiveMode string   `json:"active_mode,omitempty"`
	ShowBot    bool     `json:"show_bot,omitempty"`
	UserIDs    []string `json:"user_ids,omitempty"`
}

type ListUsersResponse struct {
	Users []User `json:"users"`
	Next  string `json:"next"`
}

func (c *client) ListUsers(r *ListUsersRequest) (ListUsersResponse, *error_utils.RestErr) {
	raw := r.params().Encode()

	//#HACK special case handling for sendbird API
	raw = strings.Replace(raw, "%2C", ",", -1)

	result := ListUsersResponse{}
	err := c.getAndReturnJSON(c.PrepareUrl(SendbirdURLUsers), raw, &result)
	if err != nil {
		logger_utils.Error(err.Error(), err)
		return ListUsersResponse{}, error_utils.NewInternalServerError("Error when trying get list users to sendbird")
	}

	return result, nil
}

type CreateUserRequest struct {
	UserID           string `json:"user_id"`
	NickName         string `json:"nickname"`
	ProfileURL       string `json:"profile_url"`
	IssueAccessToken bool   `json:"issue_access_token,omitempty"`
}

func (c *client) CreateUser(r *CreateUserRequest) (User, *error_utils.RestErr) {
	if r.UserID == "" {
		logger_utils.Error(fmt.Sprintf("%s user_id is missing", CurrentFilePath), errors.New("User ID Missing when create user to sendbird"))
		return User{}, error_utils.NewBadRequestError("user_id is missing")
	}

	result := User{}

	if err := c.postAndReturnJSON(c.PrepareUrl(SendbirdURLUsers), r, &result); err != nil {
		logger_utils.Error(fmt.Sprintf("%s : %v", CurrentFilePath, err.Error()), err)
		return User{}, error_utils.NewInternalServerError("error when creating user to vendor")
	}

	return result, nil
}
