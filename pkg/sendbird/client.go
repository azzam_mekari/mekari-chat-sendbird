package sendbird

import (
	"bytes"
	"encoding/json"
	"fmt"
	"mekari-chat-sendbird/pkg/error_utils"
	"mekari-chat-sendbird/pkg/logger_utils"
	"net/http"
	"net/url"
)

type Client interface {
	ListUsers(*ListUsersRequest) (ListUsersResponse, *error_utils.RestErr)
	CreateUser(*CreateUserRequest) (User, *error_utils.RestErr)
}

type client struct {
	app_id     string
	apiKey     string
	baseUrl    string
	httpClient *http.Client
}

func NewClient(apiKey string, app_id string) (Client, *error_utils.RestErr) {
	baseUrl := fmt.Sprintf("https://api-%s.sendbird.com/v3", app_id)

	c := &client{
		app_id:     app_id,
		apiKey:     apiKey,
		baseUrl:    baseUrl,
		httpClient: &http.Client{},
	}

	if apiKey == "" {
		return c, error_utils.NewInternalServerError("Missing Senbird API Key")
	}

	return c, nil
}

func (c *client) prepareHeader(req *http.Request) {
	req.Header.Set("Content-Type", SendbirdContentTypeHeaderValue)
	req.Header.Set(SendbirdAPITokenHeaderKey, c.apiKey)
}

func (c *client) get(config string, rawQueryString string) (*http.Response, error) {
	req, err := http.NewRequest("GET", config, nil)
	if err != nil {
		return nil, err
	}

	c.prepareHeader(req)
	req.URL.RawQuery = rawQueryString

	logger_utils.Info(fmt.Sprintf("URL: %s", req.URL.String()))

	return c.httpClient.Do(req)
}

func (c *client) post(config string, apiReq interface{}) (*http.Response, error) {

	body, err := json.Marshal(apiReq)
	if err != nil {
		return nil, err
	}

	fmt.Println("post(): requestBody=", string(body))

	req, err := http.NewRequest("POST", config, bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	c.prepareHeader(req)

	req.URL.RawQuery = url.Values{}.Encode()
	return c.httpClient.Do(req)
}

func (c *client) getAndReturnJSON(config string, rawQuery string, resp interface{}) error {
	httpResp, err := c.get(config, rawQuery)
	if err != nil {
		return err
	}

	defer httpResp.Body.Close()

	err = CheckSendbirdError(httpResp)
	if err != nil {
		return err
	}

	return json.NewDecoder(httpResp.Body).Decode(resp)
}

func (c *client) postAndReturnJSON(config string, apiReq interface{}, resp interface{}) error {
	httpResp, err := c.post(config, apiReq)
	if err != nil {
		return err
	}

	defer httpResp.Body.Close()

	err = CheckSendbirdError(httpResp)
	if err != nil {
		return err
	}

	return json.NewDecoder(httpResp.Body).Decode(resp)
}

func (c *client) PrepareUrl(pathEncodedUrl string) string {
	urlVal := &url.URL{
		Scheme:  constScheme,
		Host:    fmt.Sprintf(constHost, c.app_id),
		Path:    constVersion + pathEncodedUrl,
		RawPath: constVersion + pathEncodedUrl,
	}
	return urlVal.String()
}
