package model

// User is model
type User struct {
	ID       int64
	Fullname string
	Email    string
	Password string
	VendorID string
}
