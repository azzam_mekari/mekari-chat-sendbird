package repository

import (
	"database/sql"
	"encoding/base64"
	"fmt"
	"mekari-chat-sendbird/app/adapter/postgresql"
	"mekari-chat-sendbird/app/domain/user"
	"mekari-chat-sendbird/pkg/error_utils"
	"mekari-chat-sendbird/pkg/logger_utils"
	"mekari-chat-sendbird/pkg/sendbird"
	"os"
)

// Query to Mekari Chat database
const (
	queryCreateUser         = "INSERT INTO users(fullname, email, password, vendor_id) VALUES ($1, $2, $3, $4) RETURNING id"
	queryUpdateUserVendorID = "UPDATE users SET vendor_id = $1 WHERE id = $2"
	queryLoginUser          = "SELECT id, fullname, email, vendor_id, auth_token FROM users WHERE email = $1 AND password = $2"
	queryUpdateAuthToken    = "UPDATE users SET auth_token = $1 WHERE id = $2"
)

// This is the repository implementation of User Domain
type userRepositoryImplementation struct {
	vendorClient sendbird.Client
}

// Depedency Injection of User Repository Implementation
func NewUserRepositoryImplementation(vendorClients sendbird.Client) user.UserRepositoryInterface {
	apiKey := os.Getenv("SENBIRD_MASTER_API_TOKEN")
	app_id := "FC164ADD-CD53-499D-8C04-9F6400D1E11B"
	vendorClient, sendbirdErr := sendbird.NewClient(apiKey, app_id)
	if sendbirdErr != nil {
		panic(sendbirdErr)
	}

	return &userRepositoryImplementation{
		vendorClient: vendorClient,
	}
}

// List sendbird users
func (r *userRepositoryImplementation) List(queryParams map[string][]string) (*[]user.User, *error_utils.RestErr) {
	request := sendbird.ListUsersRequest{}

	if queryParams["limit"] != nil {
		request.Limit = queryParams["limit"][0]
	}

	responses, err := r.vendorClient.ListUsers(&request)
	if err != nil {
		return nil, err
	}

	var results []user.User

	for _, response := range responses.Users {
		results = append(results, user.User{
			Fullname: response.NickName,
			VendorID: response.UserID,
		})
	}

	return &results, nil
}

// Create user
func (r *userRepositoryImplementation) Save(user *user.User) *error_utils.RestErr {
	// Create user into mekari chat database
	stmt, err := postgresql.Client.Prepare(queryCreateUser)
	if err != nil {
		logger_utils.Error("error when trying to prepare save user statement, ", err)
		return error_utils.NewInternalServerError("database error")
	}
	defer stmt.Close()

	// Get last inserted id from postgresql
	err = stmt.QueryRow(user.Fullname, user.Email, user.Password, user.VendorID).Scan(&user.ID)
	if err != nil {
		logger_utils.Error("error when trying to save user", err)
		return error_utils.NewInternalServerError("database error")
	}

	// Create user to sendbird
	request := sendbird.CreateUserRequest{
		UserID:   user.VendorID,
		NickName: user.Fullname,
	}

	fmt.Println(request)

	_, createErr := r.vendorClient.CreateUser(&request)
	if createErr != nil {
		return createErr
	}

	return nil
}

// Get user
func (r *userRepositoryImplementation) Get() (*user.User, *error_utils.RestErr) {
	result := user.User{
		ID:       1,
		Fullname: "Azzam J U",
		Email:    "azzam.jihad@mekari.com",
		VendorID: "123",
	}

	return &result, nil
}

// Login users
func (r *userRepositoryImplementation) Login(data map[string]interface{}) (*user.User, *error_utils.RestErr) {

	if data["email"] == "" {
		return nil, error_utils.NewBadRequestError("email is required")
	}

	if data["password"] == "" {
		return nil, error_utils.NewBadRequestError("password is required")
	}

	// find user in database
	stmt, err := postgresql.Client.Prepare(queryLoginUser)
	if err != nil {
		logger_utils.Error("error when trying to prepare login user statement, ", err)
		return nil, error_utils.NewInternalServerError("database error")
	}
	defer stmt.Close()

	var user user.User
	var authToken sql.NullString
	err = stmt.QueryRow(data["email"], data["password"]).Scan(&user.ID, &user.Fullname, &user.Email, &user.VendorID, &authToken)
	if err == sql.ErrNoRows {
		return nil, error_utils.NewNotFoundError("user not found. invalid credentials.")
	}
	if err != nil {
		logger_utils.Error("error when trying to exec login user statement, ", err)
		return nil, error_utils.NewInternalServerError("database error")
	}

	if authToken.Valid {
		user.AuthToken = authToken.String
	} else {
		user.AuthToken = base64.StdEncoding.EncodeToString([]byte(user.Email))

		// Update auth token
		stmt, err = postgresql.Client.Prepare(queryUpdateAuthToken)
		if err != nil {
			logger_utils.Error("error when trying to prepare update user auth token statement, ", err)
			return nil, error_utils.NewInternalServerError("database error")
		}
		defer stmt.Close()

		_, err := stmt.Exec(user.AuthToken, user.ID)
		if err != nil {
			logger_utils.Error("error when trying to exec update user auth token statement, ", err)
			return nil, error_utils.NewInternalServerError("database error")
		}
	}

	return &user, nil
}
