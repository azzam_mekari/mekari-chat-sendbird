package repository

import (
	"mekari-chat-sendbird/app/adapter/postgresql"
	"mekari-chat-sendbird/app/domain/room"
	"mekari-chat-sendbird/pkg/error_utils"
	"mekari-chat-sendbird/pkg/logger_utils"
	"mekari-chat-sendbird/pkg/sendbird"
	"os"
)

const (
	queryCreateRoom = "INSERT INTO rooms(name, vendor_id) VALUES ($1, $2) RETURNING id"
)

// This is the repository implementation of Room Domain
type roomRepositoryImplementation struct {
	vendorClient sendbird.Client
}

// Depedency Injection of User Repository Implementation
func NewRoomRepositoryImplementation(vendorClients sendbird.Client) room.RoomRepositoryInterface {
	apiKey := os.Getenv("SENBIRD_MASTER_API_TOKEN")
	app_id := "FC164ADD-CD53-499D-8C04-9F6400D1E11B"
	vendorClient, sendbirdErr := sendbird.NewClient(apiKey, app_id)
	if sendbirdErr != nil {
		panic(sendbirdErr)
	}

	return &roomRepositoryImplementation{
		vendorClient: vendorClient,
	}
}

// Create a Room
func (r *roomRepositoryImplementation) Save(request *room.CreateRoomRequest) (*room.Room, *error_utils.RestErr) {
	// TODO: Create room into sendbird

	// Create room into mekari chat database
	stmt, err := postgresql.Client.Prepare(queryCreateRoom)
	if err != nil {
		logger_utils.Error("error when trying to prepare create room statement, ", err)
		return nil, error_utils.NewInternalServerError("database error")
	}
	defer stmt.Close()

	// Get last inserted id from postgresql
	err = stmt.QueryRow(request.Room.Name, request.Room.VendorID).Scan(&request.Room.ID)
	if err != nil {
		logger_utils.Error("error when trying to create room", err)
		return nil, error_utils.NewInternalServerError("database error")
	}
	return &request.Room, nil
}
