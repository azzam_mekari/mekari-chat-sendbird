package service

import (
	"encoding/json"
	"mekari-chat-sendbird/app/domain/user"
	"mekari-chat-sendbird/pkg/error_utils"
)

type UserService interface {
	List(map[string][]string) (*[]user.User, *error_utils.RestErr)
	Create(user.User) (*user.User, *error_utils.RestErr)
	Get() (*user.User, *error_utils.RestErr)
	Login([]byte) (*user.User, *error_utils.RestErr)
}

type userService struct {
	repository user.UserRepositoryInterface
}

func NewUserService(repo user.UserRepositoryInterface) UserService {
	return &userService{
		repository: repo,
	}
}

func (s *userService) List(queryParams map[string][]string) (*[]user.User, *error_utils.RestErr) {
	users, err := s.repository.List(queryParams)
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (s *userService) Create(user user.User) (*user.User, *error_utils.RestErr) {
	// validate user request data
	err := user.ValidateCreateUserRequest()
	if err != nil {
		return nil, err
	}

	// Pass data to repository
	if err := s.repository.Save(&user); err != nil {
		return nil, err
	}

	return &user, nil
}

func (s *userService) Get() (*user.User, *error_utils.RestErr) {
	user, err := s.repository.Get()
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *userService) Login(jsonData []byte) (*user.User, *error_utils.RestErr) {
	var data map[string]interface{}
	json.Unmarshal(jsonData, &data)

	user, err := s.repository.Login(data)
	if err != nil {
		return nil, err
	}

	return user, nil
}
