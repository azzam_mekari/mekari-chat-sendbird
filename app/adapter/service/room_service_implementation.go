package service

import (
	"encoding/json"
	"fmt"
	"mekari-chat-sendbird/app/domain/room"
	"mekari-chat-sendbird/pkg/error_utils"
	"mekari-chat-sendbird/pkg/logger_utils"
)

const (
	RoomServiceImplementationFilePath = "app/adapter/service/room_service_implementation.go"
)

type RoomService interface {
	Create([]byte) (*room.Room, *error_utils.RestErr)
}

type roomService struct {
	repository room.RoomRepositoryInterface
}

func NewRoomService(repo room.RoomRepositoryInterface) RoomService {
	return &roomService{
		repository: repo,
	}
}

func (s *roomService) Create(jsonData []byte) (*room.Room, *error_utils.RestErr) {
	// Unmarshall json
	var data map[string]interface{}
	if jsonErr := json.Unmarshal(jsonData, &data); jsonErr != nil {
		logger_utils.Error(fmt.Sprintf("%s error unmarshal json", RoomServiceImplementationFilePath), jsonErr)
		return nil, error_utils.NewInternalServerError("invalid json body")
	}

	user_ids_interface := data["user_ids"].([]interface{})
	user_ids := make([]string, len(user_ids_interface))
	for i, v := range user_ids_interface {
		user_ids[i] = v.(string)
	}

	// Build Request
	request := room.CreateRoomRequest{}
	request.Room.Name = fmt.Sprintf("%v", data["name"])
	request.Room.VendorID = fmt.Sprintf("%v", data["vendor_id"])
	request.UserIDs = user_ids

	// Pass data to repository
	room, err := s.repository.Save(&request)
	if err != nil {
		return nil, err
	}

	return room, nil
}
