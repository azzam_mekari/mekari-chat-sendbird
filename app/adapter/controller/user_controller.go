package controller

import (
	"mekari-chat-sendbird/app/adapter/service"
	"mekari-chat-sendbird/app/domain/user"
	"mekari-chat-sendbird/pkg/error_utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

type UserController interface {
	List(*gin.Context)
	Create(*gin.Context)
	Get(*gin.Context)
	Login(*gin.Context)
}

type userController struct {
	service service.UserService
}

func NewUserController(service service.UserService) UserController {
	return &userController{
		service: service,
	}
}

func (ctrl *userController) List(c *gin.Context) {
	queryParams := c.Request.URL.Query()

	users, err := ctrl.service.List(queryParams)
	if err != nil {
		c.JSON(err.Status, err)
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data":    users,
	})
}

func (ctrl *userController) Create(c *gin.Context) {
	var user user.User
	if err := c.ShouldBindJSON(&user); err != nil {
		restErr := error_utils.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status, restErr)
		return
	}

	result, saveErr := ctrl.service.Create(user)
	if saveErr != nil {
		c.JSON(saveErr.Status, saveErr)
		return
	}

	c.JSON(http.StatusCreated, result)
}

func (ctrl *userController) Get(c *gin.Context) {
	user, err := ctrl.service.Get()
	if err != nil {
		c.JSON(err.Status, err)
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data":    user,
	})
}

func (ctrl *userController) Login(c *gin.Context) {
	jsonData, jsonErr := c.GetRawData()
	if jsonErr != nil {
		c.JSON(http.StatusBadRequest, jsonErr.Error())
		return
	}

	user, err := ctrl.service.Login(jsonData)
	if err != nil {
		c.JSON(err.Status, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "success",
		"data":    user,
	})
}
