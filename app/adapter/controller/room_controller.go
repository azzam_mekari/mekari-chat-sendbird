package controller

import (
	"mekari-chat-sendbird/app/adapter/service"
	"net/http"

	"github.com/gin-gonic/gin"
)

type RoomController interface {
	Create(*gin.Context)
}

type roomController struct {
	service service.RoomService
}

func NewRoomController(service service.RoomService) RoomController {
	return &roomController{
		service: service,
	}
}

func (ctrl *roomController) Create(c *gin.Context) {
	jsonData, jsonErr := c.GetRawData()
	if jsonErr != nil {
		c.JSON(http.StatusBadRequest, jsonErr.Error())
		return
	}

	result, saveErr := ctrl.service.Create(jsonData)
	if saveErr != nil {
		c.JSON(saveErr.Status, saveErr)
		return
	}

	c.JSON(http.StatusCreated, result)
}
