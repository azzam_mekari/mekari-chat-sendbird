package room

// Room Entity
type Room struct {
	ID       int64  `json:"id"`
	Name     string `json:"name"`
	VendorID string `json:"vendor_id"`
}
