package room

import "mekari-chat-sendbird/pkg/error_utils"

// RoomRepositoryInterface is interface of user repository
type RoomRepositoryInterface interface {
	Save(*CreateRoomRequest) (*Room, *error_utils.RestErr)
}

type CreateRoomRequest struct {
	Room
	UserIDs []string `json:"user_ids"`
}

func (r CreateRoomRequest) ValidateCreateRoomRequest() *error_utils.RestErr {
	if r.Room.Name == "" {
		return error_utils.NewBadRequestError("Room name is missing")
	}

	if r.Room.VendorID == "" {
		return error_utils.NewBadRequestError("Room Vendor ID is missing")
	}
	return nil
}
