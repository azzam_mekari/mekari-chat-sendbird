package user

import (
	"mekari-chat-sendbird/pkg/error_utils"
)

// User Entity
type User struct {
	ID        int64  `json:"id"`
	Fullname  string `json:"fullname"`
	Email     string `json:"email"`
	Password  string `json:"password"`
	VendorID  string `json:"vendor_id"`
	AuthToken string `json:"auth_token"`
}

func (u User) ValidateCreateUserRequest() *error_utils.RestErr {
	if u.Fullname == "" {
		return error_utils.NewBadRequestError("User Fullname is missing")
	}

	if u.Email == "" {
		return error_utils.NewBadRequestError("User Email is missing")
	}

	if u.Password == "" {
		return error_utils.NewBadRequestError("User Password is missing")
	}

	if u.VendorID == "" {
		return error_utils.NewBadRequestError("User VendorID is missing")
	}
	return nil
}
