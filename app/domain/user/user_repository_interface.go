package user

import (
	"mekari-chat-sendbird/pkg/error_utils"
)

// UserRepositoryInterface is interface of user repository
type UserRepositoryInterface interface {
	List(map[string][]string) (*[]User, *error_utils.RestErr)
	Save(*User) *error_utils.RestErr
	Get() (*User, *error_utils.RestErr)
	Login(map[string]interface{}) (*User, *error_utils.RestErr)
}
