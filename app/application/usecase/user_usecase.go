package user

import (
	"mekari-chat-sendbird/app/domain/user"
	"mekari-chat-sendbird/pkg/error_utils"
)

// UserUseCase is the usecase of getting user
func GetUserUseCase(r user.UserRepositoryInterface) (*user.User, *error_utils.RestErr) {
	return r.Get()
}
