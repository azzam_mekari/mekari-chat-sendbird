package app

import (
	"mekari-chat-sendbird/app/adapter/controller"
	"mekari-chat-sendbird/app/adapter/repository"
	"mekari-chat-sendbird/app/adapter/service"
)

func route() {
	userController := controller.NewUserController(service.NewUserService(repository.NewUserRepositoryImplementation(vendorClient)))
	roomController := controller.NewRoomController(service.NewRoomService(repository.NewRoomRepositoryImplementation(vendorClient)))

	router.POST("/login", userController.Login)
	router.GET("/user", userController.List)
	router.POST("/user", userController.Create)
	router.GET("/user/get", userController.Get)

	router.POST("/room", roomController.Create)
}
